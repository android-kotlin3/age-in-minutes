# Age In Minutes

The next project we will practice essential bases, to interact with the interface and logic for a simple project in kotlin

## Project download

If you want to download the repository, you must follow the git clone command followed by the clone by HTTPS command:

```
git clone https://gitlab.com/android-kotlin3/age-in-minutes.git
```

***

## Project Summary

For this project, I will interact a little with the interface and practice to identify the components with the view,
add a DatePicker and calculate my age in minutes and the difference of days from the current day to the selected date,
including validating that days cannot be selected futures.

## Solution to present:

Important detail is that the project was carried out with Kotlin for the development and management of the interface state

***
![image](/markdown/1.png)
***
![image](/markdown/2.png)
***
![image](/markdown/3.png)
***
